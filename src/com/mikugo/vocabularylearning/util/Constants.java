package com.mikugo.vocabularylearning.util;

public class Constants {
    public static final String ARG_VOCABLE_LIST_NUMBER = "vocablelist_number";
    public static final String ARG_VOCABLE_LIST_ID = "vocablelist_id";
}

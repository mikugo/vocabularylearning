package com.mikugo.vocabularylearning;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;

import com.mikugo.vocabularylearning.dao.VocabularyLearningDataSource;
import com.mikugo.vocabularylearning.model.VocableList;
import com.mikugo.vocabularylearning.util.Constants;

public class CreateListActivity extends Activity {

    VocabularyLearningDataSource vocabularyLearningDataSource;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
	super.onCreate(savedInstanceState);
	setContentView(R.layout.activity_create_list);

	vocabularyLearningDataSource = new VocabularyLearningDataSource(this);
	vocabularyLearningDataSource.open();
	
	getActionBar();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
	// Inflate the menu; this adds items to the action bar if it is present.
	getMenuInflater().inflate(R.menu.create_list, menu);
	return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
	// Handle action bar item clicks here. The action bar will
	// automatically handle clicks on the Home/Up button, so long
	// as you specify a parent activity in AndroidManifest.xml.
	int id = item.getItemId();
	if (id == R.id.action_settings) {
	    return true;
	} else if (id == R.id.action_accept) {
	    long listId =  createList();
	    callToMainActivity(listId);
	    return true;
	}

	return super.onOptionsItemSelected(item);
    }

    private void callToMainActivity(long listId) {
	Intent intent = new Intent(this, MainActivity.class);
	intent.putExtra("listCreated", true);
	intent.putExtra(Constants.ARG_VOCABLE_LIST_ID, listId);
	startActivity(intent);
	overridePendingTransition(R.animator.slide_out_down, R.animator.stay);
    }

    private long createList() {
	EditText editText = (EditText) findViewById(R.id.editTextCreateList);
	String listName = editText.getText().toString();
	VocableList vocList = vocabularyLearningDataSource.createVocableList(listName);
	return vocList.getId();
    }

    @Override
    protected void onPause() {
	vocabularyLearningDataSource.close();
	super.onPause();
    }

    @Override
    protected void onResume() {
	vocabularyLearningDataSource.open();
	super.onResume();
    }
}

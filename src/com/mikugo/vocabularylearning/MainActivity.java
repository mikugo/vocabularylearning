package com.mikugo.vocabularylearning;

import java.util.List;

import android.app.Activity;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.app.ListFragment;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.mikugo.vocabularylearning.adapter.ArrayAdapterVocableList;
import com.mikugo.vocabularylearning.dao.VocabularyLearningDataSource;
import com.mikugo.vocabularylearning.fragments.VocableListFragment;
import com.mikugo.vocabularylearning.model.VocableList;
import com.mikugo.vocabularylearning.util.Constants;

public class MainActivity extends Activity {

    private String[] mExampleDrawerList;
    private DrawerLayout mDrawerLayout;
    private ListView mDrawerList;
    private RelativeLayout mLeftDrawerRelativeLayout;
    VocabularyLearningDataSource vocabularyLearningDataSource;
    private ActionBarDrawerToggle mDrawerToggle;
    private VocableList mInitialList;
    private long mInitialListId;
    List<VocableList> allVocLists;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
	super.onCreate(savedInstanceState);
	setContentView(R.layout.activity_main);

	vocabularyLearningDataSource = new VocabularyLearningDataSource(this);
	vocabularyLearningDataSource.open();

	// Only for testing - setting up example data
	// ExampleDataSetupAction.setupExampleData(vocabularyLearningDataSource);
	
	//Preparing list Data
	allVocLists = vocabularyLearningDataSource.getAllVocableLists();
	VocableList[] vocListArray = allVocLists
		.toArray(new VocableList[allVocLists.size()]);
	mDrawerList.setAdapter(new ArrayAdapterVocableList(this,
		R.layout.drawer_list_item, vocListArray));

	mDrawerList.setOnItemClickListener(new DrawerItemClickListener());
	
	
	//Preparing Navigationdrawer
	mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
	mDrawerLayout.setDrawerShadow(R.drawable.drawer_shadow,
		GravityCompat.START);

	mLeftDrawerRelativeLayout = (RelativeLayout) findViewById(R.id.left_drawer_relative_layout);
	mDrawerList = (ListView) findViewById(R.id.left_drawer);

	// initializing Action Bar
	// enable ActionBar app icon to behave as action to toggle nav drawer
	getActionBar().setDisplayHomeAsUpEnabled(true);
	getActionBar().setHomeButtonEnabled(true);

	// ActionBarDrawerToggle ties together the the proper interactions
	// between the sliding drawer and the action bar app icon
	mDrawerToggle = new ActionBarDrawerToggle(this, /* host Activity */
	mDrawerLayout, /* DrawerLayout object */
	R.drawable.ic_drawer, /* nav drawer image to replace 'Up' caret */
	R.string.drawer_open, /* "open drawer" description for accessibility */
	R.string.drawer_close /* "close drawer" description for accessibility */
	) {
	    public void onDrawerClosed(View view) {
		// getActionBar().setTitle(mTitle);
		invalidateOptionsMenu(); // creates call to
					 // onPrepareOptionsMenu()
	    }

	    public void onDrawerOpened(View drawerView) {
		// getActionBar().setTitle(mDrawerTitle);
		invalidateOptionsMenu(); // creates call to
					 // onPrepareOptionsMenu()
	    }
	};

	mDrawerLayout.setDrawerListener(mDrawerToggle);

	if (savedInstanceState == null) {
	    // selectItem(0);
	}

	// initial list id if new list created - in the right section should be
	// displayed this list
	initializeMainContent();
    }

    private void initializeMainContent() {
	Intent intent = getIntent();

	mInitialListId = intent.getLongExtra(Constants.ARG_VOCABLE_LIST_ID, -1);
	if (mInitialListId > -1) {

	    mInitialList = vocabularyLearningDataSource
		    .getVocableList(mInitialListId);

	    selectItem(allVocLists.size() - 1, mInitialListId,
		    mInitialList.getName());

	} else if (allVocLists.size() > 0) {

	    mInitialListId = allVocLists.get(0).getId();

	    mInitialList = vocabularyLearningDataSource
		    .getVocableList(mInitialListId);

	    selectItem(0, mInitialListId, mInitialList.getName());
	}

    }

    /* The click listner for ListView in the navigation drawer */
    private class DrawerItemClickListener implements
	    ListView.OnItemClickListener {
	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position,
		long id) {

	    Context context = view.getContext();
	    // TextView textViewVocList = view.findViewById(R.id.left_drawer);
	    TextView textView = (TextView) view;
	    Log.d("ITEM CLICKED ID: ", textView.getTag().toString() + " "
		    + textView.getText().toString());

	    selectItem(position, Long.valueOf(textView.getTag().toString()),
		    textView.getText().toString());
	}

    }

    private void selectItem(int position, long vocableListId, String text) {
	// update the main content by replacing fragments
	ListFragment listFragment = new VocableListFragment();

	Bundle args = new Bundle();

	args.putInt(Constants.ARG_VOCABLE_LIST_NUMBER, position);
	args.putLong(Constants.ARG_VOCABLE_LIST_ID, vocableListId);
	listFragment.setArguments(args);

	FragmentManager fragmentManager = getFragmentManager();
	// fragmentManager.beginTransaction().replace(R.id.content_frame,
	// listFragment).commit();
	FragmentTransaction fragmentTransaction = fragmentManager
		.beginTransaction();
	fragmentTransaction.replace(R.id.content_frame, listFragment);
	fragmentTransaction.addToBackStack(null);
	fragmentTransaction.commit();

	// update selected item and title, then close the drawer
	mDrawerList.setItemChecked(position, true);

	setTitle(text);
	mDrawerLayout.closeDrawer(mLeftDrawerRelativeLayout);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
	// Inflate the menu; this adds items to the action bar if it is present.
	getMenuInflater().inflate(R.menu.main, menu);
	return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
	// The action bar home/up action should open or close the drawer.
	// ActionBarDrawerToggle will take care of this.
	if (mDrawerToggle.onOptionsItemSelected(item)) {
	    return true;
	}

	// Handle action bar item clicks here. The action bar will
	// automatically handle clicks on the Home/Up button, so long
	// as you specify a parent activity in AndroidManifest.xml.
	int id = item.getItemId();
	if (id == R.id.action_settings) {
	    return true;
	}
	return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onPause() {
	vocabularyLearningDataSource.close();
	super.onPause();
    }

    @Override
    protected void onResume() {
	vocabularyLearningDataSource.open();
	super.onResume();
    }

    /**
     * When using the ActionBarDrawerToggle, you must call it during
     * onPostCreate() and onConfigurationChanged()...
     */
    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
	super.onPostCreate(savedInstanceState);
	// Sync the toggle state after onRestoreInstanceState has occurred.
	mDrawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
	super.onConfigurationChanged(newConfig);
	// Pass any configuration change to the drawer toggls
	mDrawerToggle.onConfigurationChanged(newConfig);
    }

    public void callCreateListActivity(View view) {
	Intent intent = new Intent(this, CreateListActivity.class);
	startActivity(intent);
	overridePendingTransition(R.animator.slide_in_up, R.animator.stay);
    }

}

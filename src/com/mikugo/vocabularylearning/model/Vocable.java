package com.mikugo.vocabularylearning.model;

import com.mikugo.vocabularylearning.sqlite.VocabularyLearningContract.VocableEntry;

public class Vocable {
    private long id;
    private String question;
    private String answer;
    private long listId;

    public long getId() {
	return id;
    }

    public void setId(long id) {
	this.id = id;
    }

    public String getQuestion() {
	return question;
    }

    public void setQuestion(String question) {
	this.question = question;
    }

    public String getAnswer() {
	return answer;
    }

    public void setAnswer(String answer) {
	this.answer = answer;
    }

    public long getListId() {
	return listId;
    }

    public void setListId(long listId) {
	this.listId = listId;
    }

}

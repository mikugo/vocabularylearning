package com.mikugo.vocabularylearning.dao;

import java.util.ArrayList;
import java.util.List;

import com.mikugo.vocabularylearning.model.Vocable;
import com.mikugo.vocabularylearning.sqlite.VocabularyLearningDBHelper;
import com.mikugo.vocabularylearning.sqlite.VocabularyLearningContract.VocableEntry;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

public class VocableDataSource {
	private SQLiteDatabase database;
	private VocabularyLearningDBHelper dbHelper;

	private String[] allColumns = { VocableEntry._ID,
			VocableEntry.COLUMN_NAME_VOCABLE_LIST_ID,
			VocableEntry.COLUMN_NAME_ENTRY_QUESTION,
			VocableEntry.COLUMN_NAME_ENTRY_ANSWER };

	public VocableDataSource(Context context) {
		dbHelper = new VocabularyLearningDBHelper(context);
	}

	public void open() throws SQLException {
		database = dbHelper.getWritableDatabase();
	}

	public void close() {
		dbHelper.close();
	}

	public Vocable createVocable(int listId, String question, String answer) {
		ContentValues contentValues = new ContentValues();
		contentValues.put(VocableEntry.COLUMN_NAME_VOCABLE_LIST_ID, listId);
		contentValues.put(VocableEntry.COLUMN_NAME_ENTRY_QUESTION, question);
		contentValues.put(VocableEntry.COLUMN_NAME_ENTRY_ANSWER, answer);

		long insertId = database.insert(VocableEntry.TABLE_NAME, null,
				contentValues);

		Cursor cursor = database.query(VocableEntry.TABLE_NAME, allColumns,
				VocableEntry._ID + " = " + insertId, null, null, null, null);
		cursor.moveToFirst();
		Vocable newVocable = cursorToVocable(cursor);
		cursor.close();
		return newVocable;
	}

	public List<Vocable> getVocableByListId(int listId) {
		List<Vocable> vocables = new ArrayList<>();

		Cursor cursor = database.query(VocableEntry.TABLE_NAME, allColumns,
				"WHERE" + VocableEntry.COLUMN_NAME_VOCABLE_LIST_ID + " = "
						+ listId, null, null, null, null);
		
		while (cursor.isAfterLast()) {
			Vocable vocabular = cursorToVocable(cursor);
			vocables.add(vocabular);
			cursor.moveToNext();
		}
		
		cursor.close();
		
		return vocables;
	}
	
	private Vocable cursorToVocable(Cursor cursor) {
		Vocable vocable = new Vocable();
		vocable.setId(cursor.getLong(0));
		vocable.setListId(cursor.getLong(1));
		vocable.setQuestion(cursor.getString(2));
		vocable.setAnswer(cursor.getString(3));

		return vocable;
	}
	
	
}

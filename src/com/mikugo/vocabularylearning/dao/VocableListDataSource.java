package com.mikugo.vocabularylearning.dao;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

import com.mikugo.vocabularylearning.model.Vocable;
import com.mikugo.vocabularylearning.model.VocableList;
import com.mikugo.vocabularylearning.sqlite.VocabularyLearningContract.VocableListEntry;
import com.mikugo.vocabularylearning.sqlite.VocabularyLearningDBHelper;
import com.mikugo.vocabularylearning.sqlite.VocabularyLearningContract.VocableEntry;

public class VocableListDataSource {
    private SQLiteDatabase database;
    private VocabularyLearningDBHelper dbHelper;

    private String[] allColumns = { VocableListEntry._ID,
	    VocableListEntry.COLUMN_NAME_LIST_NAME };

    public VocableListDataSource(Context context) {
	dbHelper = new VocabularyLearningDBHelper(context);
    }

    public void open() throws SQLException {
	database = dbHelper.getWritableDatabase();
    }

    public void close() {
	dbHelper.close();
    }

    public List<VocableList> getAllVocableLists() {
	List<VocableList> allLists = new ArrayList<>();

	Cursor cursor = database.query(VocableListEntry.TABLE_NAME, allColumns,
		null, null, null, null, null);

	if (cursor.moveToFirst()) {
	    while (!cursor.isAfterLast()) {
		VocableList vocList = cursorToVocableList(cursor);
		allLists.add(vocList);
		cursor.moveToNext();
	    }
	}

	cursor.close();
	return allLists;
    }

    public VocableList createVocableList(String name) {
	ContentValues contentValues = new ContentValues();
	contentValues.put(VocableListEntry.COLUMN_NAME_LIST_NAME, name);

	long insertId = database.insert(VocableListEntry.TABLE_NAME, null,
		contentValues);

	Cursor cursor = database
		.query(VocableListEntry.TABLE_NAME, allColumns,
			VocableListEntry._ID + " = " + insertId, null, null,
			null, null);

	cursor.moveToFirst();
	VocableList newVocableList = cursorToVocableList(cursor);
	cursor.close();
	return newVocableList;
    }

    public VocableList getVocableList(long listId) {

	Cursor cursor = database.query(VocableListEntry.TABLE_NAME, allColumns,
		"WHERE" + VocableListEntry._ID + "=" + listId, null, null,
		null, null);

	VocableList vocableList = cursorToVocableList(cursor);
	return vocableList;
    }

    public void deleteAllRows() {
	database.delete(VocableListEntry.TABLE_NAME, null, null);
    }

    private VocableList cursorToVocableList(Cursor cursor) {
	VocableList vocableList = new VocableList();
	vocableList.setId(cursor.getLong(0));
	vocableList.setName(cursor.getString(1));

	return vocableList;
    }

}

package com.mikugo.vocabularylearning.dao;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

import com.mikugo.vocabularylearning.model.Vocable;
import com.mikugo.vocabularylearning.model.VocableList;
import com.mikugo.vocabularylearning.sqlite.VocabularyLearningDBHelper;
import com.mikugo.vocabularylearning.sqlite.VocabularyLearningContract.VocableEntry;
import com.mikugo.vocabularylearning.sqlite.VocabularyLearningContract.VocableListEntry;

public class VocabularyLearningDataSource {
    private SQLiteDatabase database;
    private VocabularyLearningDBHelper dbHelper;

    private String[] allVocableListColumns = { VocableListEntry._ID,
	    VocableListEntry.COLUMN_NAME_LIST_NAME };

    private String[] allVocableColumns = { VocableEntry._ID,
	    VocableEntry.COLUMN_NAME_VOCABLE_LIST_ID,
	    VocableEntry.COLUMN_NAME_ENTRY_QUESTION,
	    VocableEntry.COLUMN_NAME_ENTRY_ANSWER };

    public VocabularyLearningDataSource(Context context) {
	dbHelper = new VocabularyLearningDBHelper(context);
    }

    public void open() throws SQLException {
	database = dbHelper.getWritableDatabase();
    }

    public void close() {
	dbHelper.close();
    }

    // ------ Methods for Vocable Lists ------ //

    public List<VocableList> getAllVocableLists() {
	List<VocableList> allLists = new ArrayList<>();

	Cursor cursor = database.query(VocableListEntry.TABLE_NAME,
		allVocableListColumns, null, null, null, null, null);

	if (cursor.moveToFirst()) {
	    while (!cursor.isAfterLast()) {
		VocableList vocList = cursorToVocableList(cursor);
		allLists.add(vocList);
		cursor.moveToNext();
	    }
	}

	cursor.close();
	return allLists;
    }

    public void deleteAllVocableListRows() {
	database.delete(VocableListEntry.TABLE_NAME, null, null);
    }

    private VocableList cursorToVocableList(Cursor cursor) {
	VocableList vocableList = new VocableList();
	vocableList.setId(cursor.getLong(0));
	vocableList.setName(cursor.getString(1));

	return vocableList;
    }

    public VocableList createVocableList(String name) {
	ContentValues contentValues = new ContentValues();
	contentValues.put(VocableListEntry.COLUMN_NAME_LIST_NAME, name);

	long insertId = database.insert(VocableListEntry.TABLE_NAME, null,
		contentValues);


	Cursor cursor = database.query(VocableListEntry.TABLE_NAME,
		allVocableListColumns, VocableListEntry._ID + " = " + insertId,
		null, null, null, null);


	cursor.moveToFirst();
	VocableList newVocableList = cursorToVocableList(cursor);
	cursor.close();
	return newVocableList;
    }

    public VocableList getVocableList(long listId) {
	
	String selectQuery = "SELECT * FROM " + VocableListEntry.TABLE_NAME
		+ " WHERE " + VocableListEntry._ID+ " = "
		+ listId;
		
	Cursor cursor = database.rawQuery(selectQuery, null);
	cursor.moveToFirst();
	
	VocableList vocableList = cursorToVocableList(cursor);
	cursor.close();
	return vocableList;
    }

    // ------ Methods for Vocables/Words ------ //

    public Vocable createVocable(long listId, String question, String answer) {
	ContentValues contentValues = new ContentValues();
	contentValues.put(VocableEntry.COLUMN_NAME_VOCABLE_LIST_ID, listId);
	contentValues.put(VocableEntry.COLUMN_NAME_ENTRY_QUESTION, question);
	contentValues.put(VocableEntry.COLUMN_NAME_ENTRY_ANSWER, answer);

	long insertId = database.insert(VocableEntry.TABLE_NAME, null,
		contentValues);

	Cursor cursor = database.query(VocableEntry.TABLE_NAME,
		allVocableColumns, VocableEntry._ID + " = " + insertId, null,
		null, null, null);
	cursor.moveToFirst();
	Vocable newVocable = cursorToVocable(cursor);
	cursor.close();
	return newVocable;
    }

    public List<Vocable> getVocablesByListId(long listId) {
	List<Vocable> vocables = new ArrayList<>();

	// Cursor cursor = database.query(VocableEntry.TABLE_NAME,
	// allVocableColumns, " WHERE "
	// + VocableEntry.COLUMN_NAME_VOCABLE_LIST_ID + " = "
	// + listId, null, null, null, null);

	String selectQuery = "SELECT * FROM " + VocableEntry.TABLE_NAME
		+ " WHERE " + VocableEntry.COLUMN_NAME_VOCABLE_LIST_ID + " = "
		+ listId;

	Cursor cursor = database.rawQuery(selectQuery, null);

	if (cursor.moveToFirst()) {
	    while (!cursor.isAfterLast()) {
		Vocable vocabular = cursorToVocable(cursor);
		vocables.add(vocabular);
		cursor.moveToNext();
	    }
	}

	cursor.close();

	return vocables;
    }

    private Vocable cursorToVocable(Cursor cursor) {
	Vocable vocable = new Vocable();
	vocable.setId(cursor.getLong(0));
	vocable.setListId(cursor.getLong(1));
	vocable.setQuestion(cursor.getString(2));
	vocable.setAnswer(cursor.getString(3));

	return vocable;
    }

}

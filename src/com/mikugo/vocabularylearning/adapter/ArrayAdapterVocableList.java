package com.mikugo.vocabularylearning.adapter;

import com.mikugo.vocabularylearning.model.VocableList;

import android.R;
import android.app.Activity;
import android.content.Context;
import android.text.Layout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class ArrayAdapterVocableList extends ArrayAdapter<VocableList> {
	Context mContext;
	int layoutResourceId;
	VocableList vocLists[];
	
	public ArrayAdapterVocableList (Context context, int layoutResourceId, VocableList[] vocLists) {
		super(context, layoutResourceId, vocLists);
		
		this.layoutResourceId = layoutResourceId;
		this.mContext = context;
		this.vocLists = vocLists;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		
		if(convertView == null) {
			//inflate the Layout
			LayoutInflater inflater = ((Activity) mContext).getLayoutInflater();
			convertView = inflater.inflate(layoutResourceId, parent, false);
		}
		
		VocableList vocList = vocLists[position];
		
		TextView textViewItem = (TextView) convertView.findViewById(R.id.text1);
		textViewItem.setText(vocList.getName() + " ID: " + vocList.getId());
		textViewItem.setTag(vocList.getId());
		
		return convertView;
	}
	
	
}

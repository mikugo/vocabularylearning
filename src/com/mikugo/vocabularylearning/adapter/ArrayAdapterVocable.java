package com.mikugo.vocabularylearning.adapter;

import android.R;
import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.mikugo.vocabularylearning.model.Vocable;
import com.mikugo.vocabularylearning.model.VocableList;

public class ArrayAdapterVocable extends ArrayAdapter<Vocable> {

    Context mContext;
    int layoutResourceId;
    Vocable[] vocables;

    public ArrayAdapterVocable(Context context, int layoutResourceId,
	    Vocable[] vocables) {
	super(context, layoutResourceId, vocables);

	this.layoutResourceId = layoutResourceId;
	this.mContext = context;
	this.vocables = vocables;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

	if (convertView == null) {
	    // inflate the Layout
	    LayoutInflater inflater = ((Activity) mContext).getLayoutInflater();
	    convertView = inflater.inflate(layoutResourceId, parent, false);
	}

	Vocable vocable = vocables[position];

	TextView textViewQuestion = (TextView) convertView
		.findViewById(R.id.text1);
	textViewQuestion.setText(vocable.getQuestion());
	textViewQuestion.setTag(vocable.getId());

	TextView textViewAnswer = (TextView) convertView
		.findViewById(R.id.text2);
	textViewAnswer.setText(vocable.getAnswer());
	textViewAnswer.setTag(vocable.getId());

	// TextView textViewItem = (TextView)
	// convertView.findViewById(R.id.text1);
	// textViewItem.setText(vocList.getName() + " ID: " + vocList.getId());
	// textViewItem.setTag(vocList.getId());

	return convertView;
    }
}

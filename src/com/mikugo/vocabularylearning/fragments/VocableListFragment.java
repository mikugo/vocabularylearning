package com.mikugo.vocabularylearning.fragments;

import java.util.ArrayList;
import java.util.List;

import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.app.ListFragment;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.mikugo.vocabularylearning.R;
import com.mikugo.vocabularylearning.adapter.ArrayAdapterVocable;
import com.mikugo.vocabularylearning.adapter.ArrayAdapterVocableList;
import com.mikugo.vocabularylearning.dao.VocabularyLearningDataSource;
import com.mikugo.vocabularylearning.model.Vocable;
import com.mikugo.vocabularylearning.util.Constants;

public class VocableListFragment extends ListFragment {

    private VocabularyLearningDataSource vocableListDataSource;
    long vocListId;

    public VocableListFragment() {
	// Empty constructor required for fragment subclasses
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
	super.onActivityCreated(savedInstanceState);

	Bundle args = getArguments();
	vocListId = args.getLong(Constants.ARG_VOCABLE_LIST_ID);

	vocableListDataSource = new VocabularyLearningDataSource(getActivity());
	vocableListDataSource.open();

	List<Vocable> vocables = vocableListDataSource
		.getVocablesByListId(vocListId);
	
	Vocable[] vocablesArray = vocables.toArray(new Vocable[vocables.size()]);
	List<String> vocableStrings = new ArrayList<>();

	for (Vocable voc : vocables) {
	    vocableStrings.add(voc.getQuestion());
	}

	String[] values = vocableStrings.toArray(new String[vocableStrings
		.size()]);

	// ArrayAdapter<String> adapter = new
	// ArrayAdapter<String>(getActivity(),
	// android.R.layout.simple_list_item_1, values);
	ArrayAdapterVocable adapter = new ArrayAdapterVocable(getActivity(),
		R.layout.voc_list_item, vocablesArray);

	setListAdapter(adapter);

	setHasOptionsMenu(true);
    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
	Toast.makeText(getActivity(), position + ". Item Clicked",
		Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
	inflater.inflate(R.menu.main_fragment, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
	int id = item.getItemId();
	if (id == R.id.action_new) {
	    callCreateVocableFragment(vocListId);
	}

	return super.onOptionsItemSelected(item);
    }

    private void callCreateVocableFragment(long listId) {
	CreateVocableFragment fragment = new CreateVocableFragment();
	Bundle bundle = new Bundle();
	bundle.putLong(Constants.ARG_VOCABLE_LIST_ID, listId);
	fragment.setArguments(bundle);
	FragmentManager fragmentManager = getFragmentManager();
	FragmentTransaction fragmentTransaction = fragmentManager
		.beginTransaction();
	fragmentTransaction.addToBackStack(null);
	fragmentTransaction.replace(this.getId(), fragment);
	fragmentTransaction.commit();
    }

    @Override
    public void onPause() {
	vocableListDataSource.close();
	super.onPause();
    }

    @Override
    public void onResume() {
	vocableListDataSource.open();
	super.onResume();
    }

}
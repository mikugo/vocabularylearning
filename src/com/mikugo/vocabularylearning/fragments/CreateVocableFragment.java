package com.mikugo.vocabularylearning.fragments;

import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.mikugo.vocabularylearning.R;
import com.mikugo.vocabularylearning.dao.VocabularyLearningDataSource;
import com.mikugo.vocabularylearning.util.Constants;

public class CreateVocableFragment extends Fragment {

    VocabularyLearningDataSource vocabularyLearningDataSource;
    long listId;

    public CreateVocableFragment() {
	// Empty constructor required for fragment subclasses
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
	super.onCreate(savedInstanceState);

	vocabularyLearningDataSource = new VocabularyLearningDataSource(getActivity());
	vocabularyLearningDataSource.open();
	
	Bundle bundle = this.getArguments();
	listId = bundle.getLong(Constants.ARG_VOCABLE_LIST_ID);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
	int id = item.getItemId();
	if (id == R.id.action_vocable_accept) {

	    EditText editTextQuestion = (EditText) getView().findViewById(
		    R.id.editTextQuestion);
	    EditText editTextAnswer = (EditText) getView().findViewById(
		    R.id.editTextAnswer);
	    String question = editTextQuestion.getText().toString();
	    String answer = editTextAnswer.getText().toString();
	    createVocable(question, answer);

	    callVocableListFragment();
	}
	return super.onOptionsItemSelected(item);
    }

    private void callVocableListFragment() {
	VocableListFragment fragment = new VocableListFragment();
	Bundle bundle = new Bundle();
	bundle.putLong(Constants.ARG_VOCABLE_LIST_ID, listId);
	fragment.setArguments(bundle);
	FragmentManager fragmentManager = getFragmentManager();
	FragmentTransaction fragmentTransaction = fragmentManager
		.beginTransaction();
	fragmentTransaction.replace(this.getId(), fragment);
	fragmentTransaction.commit();
	
    }


    private void createVocable(String question, String answer) {
	vocabularyLearningDataSource.createVocable(listId, question, answer);

    }

    @Override
    public View onCreateView(LayoutInflater inflater,
	    @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
	return inflater.inflate(R.layout.fragment_add_new_vocabular, container,
		false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
	super.onActivityCreated(savedInstanceState);

	setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
	inflater.inflate(R.menu.create_vocable, menu);
	menu.removeItem(R.id.action_new);
    }
    
    @Override
    public void onPause() {
	vocabularyLearningDataSource.close();
	super.onPause();
    }

    @Override
    public void onResume() {
	vocabularyLearningDataSource.open();
	super.onResume();
    }
}

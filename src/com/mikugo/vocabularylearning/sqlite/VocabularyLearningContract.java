package com.mikugo.vocabularylearning.sqlite;

import android.provider.BaseColumns;

public final class VocabularyLearningContract {
	public VocabularyLearningContract () {}
	
	public static abstract class VocableEntry implements BaseColumns {
		public static final String TABLE_NAME = "vocable";
		//public static final String COLUMN_NAME_ENTRY_ID = "entryid";
		public static final String COLUMN_NAME_ENTRY_QUESTION = "question";
		public static final String COLUMN_NAME_ENTRY_ANSWER = "answer";
		public static final String COLUMN_NAME_VOCABLE_LIST_ID = "listid";
	}
	
	public static abstract class VocableListEntry implements BaseColumns {
		public static final String TABLE_NAME = "vocablelist";
		//public static final String COLUMN_NAME_ENTRY_ID = "listid";
		public static final String COLUMN_NAME_LIST_NAME = "name";
	}
}

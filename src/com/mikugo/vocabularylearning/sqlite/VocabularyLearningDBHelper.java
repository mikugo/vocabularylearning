package com.mikugo.vocabularylearning.sqlite;

import com.mikugo.vocabularylearning.sqlite.VocabularyLearningContract.VocableEntry;
import com.mikugo.vocabularylearning.sqlite.VocabularyLearningContract.VocableListEntry;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;

public class VocabularyLearningDBHelper extends SQLiteOpenHelper {
	public static final int DATABASE_VERSION = 1;
	public static final String DATABASE_NAME = "VocabularyLearning.db";

	private static final String TEXT_TYPE = " TEXT";
	private static final String COMMA_SEP = ",";

	private static final String SQL_CREATE_VOCABLE_TABLE = "CREATE TABLE "
			+ VocableEntry.TABLE_NAME + " ("
			+ VocableEntry._ID
			+ " INTEGER PRIMARY KEY AUTOINCREMENT,"
			// + VocableEntry.COLUMN_NAME_ENTRY_ID + TEXT_TYPE + COMMA_SEP
			+ VocableEntry.COLUMN_NAME_VOCABLE_LIST_ID + " INTEGER" + COMMA_SEP
			+ VocableEntry.COLUMN_NAME_ENTRY_QUESTION + TEXT_TYPE + COMMA_SEP
			+ VocableEntry.COLUMN_NAME_ENTRY_ANSWER + TEXT_TYPE
			+ " );";

	private static final String SQL_CREATE_VOCABLE_LIST_TABLE = "CREATE TABLE "
			+ VocableListEntry.TABLE_NAME + " (" + VocableListEntry._ID
			+ " INTEGER PRIMARY KEY AUTOINCREMENT,"
			+ VocableListEntry.COLUMN_NAME_LIST_NAME + TEXT_TYPE
			+ " );";

	private static final String SQL_DELETE_VOCABLES = "DROP TABLE IF EXISTS"
			+ VocableEntry.TABLE_NAME;
	private static final String SQL_DELETE_VOCABLE_LISTS = "DROP TABLE IF EXISTS"
			+ VocableListEntry.TABLE_NAME;

	public VocabularyLearningDBHelper(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		db.execSQL(SQL_CREATE_VOCABLE_TABLE);
		db.execSQL(SQL_CREATE_VOCABLE_LIST_TABLE);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		db.execSQL(SQL_DELETE_VOCABLES);
		db.execSQL(SQL_DELETE_VOCABLE_LISTS);
		onCreate(db);
	}
	
	@Override
	public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		onUpgrade(db, oldVersion, newVersion);
	}

	@Override
	public void onOpen(SQLiteDatabase db) {
		super.onOpen(db);
		// if(!db.isReadOnly()) {
		// Enable foreign key constraints
		// db.execSQL("PRAGMA foreign_keys=ON;");
		// }
	}

}

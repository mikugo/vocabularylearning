package com.mikugo.vocabularylearning.sqlite;

import android.content.Context;

import com.mikugo.vocabularylearning.dao.VocableDataSource;
import com.mikugo.vocabularylearning.dao.VocableListDataSource;
import com.mikugo.vocabularylearning.dao.VocabularyLearningDataSource;

public class ExampleDataSetupAction {
	
	public static void setupExampleData(VocabularyLearningDataSource dataSource) {
		
	    	//Creating Lists
	    	dataSource.createVocableList("Liste 1");
	    	dataSource.createVocableList("Liste 2");
	    	dataSource.createVocableList("Liste 3");
	    	dataSource.createVocableList("Liste 4");
	    	dataSource.createVocableList("Liste 5");
	    	dataSource.createVocableList("Liste 6");
	    	dataSource.createVocableList("Liste 7");
	    	dataSource.createVocableList("Liste 8");
	    	dataSource.createVocableList("Liste 9");
	    	dataSource.createVocableList("Liste 10");
	    	dataSource.createVocableList("Liste 11");
	    	dataSource.createVocableList("Liste 12");
	    	dataSource.createVocableList("Liste 13");
	    	dataSource.createVocableList("Liste 14");
	    	dataSource.createVocableList("Liste 15");
	    	dataSource.createVocableList("Liste 16");
	    	dataSource.createVocableList("Liste 17");
	    	dataSource.createVocableList("Liste 18");
	    	dataSource.createVocableList("Liste 19");
	    
	    	//Creating vocables related to Lists
	    	dataSource.createVocable(1, "Mouse", "Maus");
		dataSource.createVocable(1, "cat", "Katze");
		dataSource.createVocable(1, "dog", "Dog");
		dataSource.createVocable(1, "bird", "vogel");
		dataSource.createVocable(2, "chair", "Stuhl");
		dataSource.createVocable(2, "table", "Tisch");
		dataSource.createVocable(3, "go", "gehen");
		dataSource.createVocable(3, "run", "rennen");
		dataSource.createVocable(4, "dance", "tanzen");
		dataSource.createVocable(5, "follow", "folgen");
	}
}
